# EXAMEN CONCEPTION LOGICIEL

## Description de l'application 

Bienvenue dans  l'application 👋 !

Je vous rappelle qu'elle a pour objectif de permettre à l'utilisateur de générer une playliste aléatoire de musiques de ses artistes préférés. Pour ce faire, l'utilisateur a juste besoin de renseigner les noms de ses artistes préférés. Le webservice développé fournit l'information nécéssaire en se basant sur deux API:

- [ ] [AudioDB](https://www.theaudiodb.com/api_guide.php) grand webservice servant de banque de données sur les informations sur des artistes, albums, titres.

- [ ] [Lyricsovh](https://lyricsovh.docs.apiary.io/) permet pour un artiste, et un titre de trouver les paroles associées au titre.

Le client fournit un fichier json avec le nom de ses artistes préférés et le webservice requète les API pour renvoyer la playlist demandée.

### Architecture

```mermaid
graph TD;
Client-->Serveur;
Serveur --> LyricsOVH;
Serveur --> AudioDB;
```

## Installation 

Cette application a été developpée sous Python3.9 depuis Datalab/VisualStudioCode. 

### Cloner le répertoire

Ceci se fait via la commande 
```
git clone https://gitlab.com/yann-ens/conception_logiciel.git
```

### Installation les modules
Ensuite, il est necessaire d'installer les libraries utiles (indispensable) au fonctionnement de l'app via la commande ci-dessous. Pour installer les librairies utiles pour l'architecture client, il faut lancer le code suivant :
```
cd conception_logiciel
cd Client
pip install -r requirements.txt
```
Pour installer celles utiles au webservice, il faut lancer le code suivant:
```
cd ..
cd serveur
pip install -r requirements.txt
```
### Configuration de l'environnement

Dans le fichier .env, vous devez copier coller la ligne ci-dessous. 
```
Audio_DB="https://www.theaudiodb.com/api/v1/json/2/"
Lyricsovh = "https://api.lyrics.ovh/v1/"
URL="http://127.0.0.1:8000"
```
De plus, vous devez fournir un fichier au format json sous cette forme de sorte à obtenir la playlist souhaité. Pour exemple, 
```
[
{
    "artiste": "daft punk",
    "note": 18
},
{
    "artiste":"gloria gaynor",
    "note": 10
}
]
```
## Vous êtes prêt !

Dans le but de générer une playlist, vous devez utiliser le webservice. Pour ceci, il faut lancer le fichier main situé de la partie Serveur et dans un autre terminal, lancer celui de la partie Client.

## Tests unitaires

Nous avons testé la méthode get_id_artist() qui permet de récupérer l'identifiant d'un artiste. Egalement get_album_artist_by_id qui permet d'obtenir l'id de l'album d'un artiste choisi. Pour tester le fonctionnement de notre webservice, il faut exécuter le bout de code suivant dans le terminal:
```
cd Serveur
python -m unittest test.py
```
