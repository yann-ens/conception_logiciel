
import json
import os
import requests
import random
from dotenv import load_dotenv


load_dotenv()

servurl = os.environ.get("URL")
with open("rudy.json", encoding="utf8") as f:
    data = json.load(f)


def work(): #pour tester le bon fonctionnment de l'url
    r = requests.get(servurl)
    return {"status_code":r.status_code}
        
    
def generate_playlist() : 
    artistes = []
    for j in range(len(data)):
        artistes.append(data[j]["artiste"])

    playlist = dict()

    for i in range(20):
        
        gen_artiste = artistes[random.randrange(0, len(artistes))] #random.sample(artistes, 20, *, counts=None) pour une sélection distincte
        play = requests.get(servurl+"/random/"+gen_artiste).json()
        playlist[i] = play["lyrics"]
        
    return playlist

if __name__ == "__main__":
   
   if work()["status_code"] == 200:
      with open("Playlist_rudy.json","w") as output:
           json.dump(generate_playlist(), output)
   else:
       print("Reessayez plus tard")

