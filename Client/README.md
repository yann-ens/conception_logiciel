# Client

La partie Client permet à l'utilisateur d'interagir avec le webservice développé. Le fichier des artistes préférés du client sont donc répertoriés dans le dossier client. Le fichier .env générique doit aussi être renseigné.

Par suite, il faut exécuter ces codes dans le terminal :
```
cd Client
python main.py
```
La playlist est enregistrée dans le fichier Playlist_rudy.json :)