
from unittest import TestCase
from fastapi.testclient import TestClient
from main import *


class testWebservice(TestCase):
    def test_get_id_artist(self):
        #given
        id_artist = "112884"
        #when
        get_id_artist_ = get_id_artist("rick astley")
        #then
        self.assertEqual(id_artist,get_id_artist_)
    def test_get_album_artist_by_id(self):
        #given
        id_album="2121296"
        #when
        get_id_album_ = get_id_album("rick astley")
        #then
        self.assertEqual(id_album,get_id_album_)
    def test_get_album_artist_by_false_id(self):
        #given
        id_album_f="2121296545555"
        #when
        get_album = get_id_album("rick astley")
        #then
        self.assertNotEqual(id_album_f,get_album)

    def test_app(self):
        client = TestClient(app)
        response = client.get('/')
        assert response.status_code == 200