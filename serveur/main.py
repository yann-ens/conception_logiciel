from typing import Optional
from fastapi import FastAPI
import requests
import json
import uvicorn
from dotenv import load_dotenv
import os
import random

load_dotenv()
Audio_DB = os.environ.get("Audio_DB")
searchArtist = "search.php?s="
searchAlbum = "album.php?i="
SearchTrack = "track.php?m="
Lyricsovh = os.environ.get("Lyricsovh")
app = FastAPI()

@app.get("/")
def start():
    """
    teste la bonne configuration et la disponibilité des services dont dépend l'API 
   Returns:
   dictionnaire qui contien les status de serveur
    """
    url_AudioDB=Audio_DB + searchArtist + "rick astley"
    rAudioDB = requests.get(url_AudioDB)
    url_Lyrics = Lyricsovh + "Rick Astley" + "/" + "Never Gonna Give You Up"
    rLyrics = requests.get(url_Lyrics)
   
    return {"AudioDB_status_code":rAudioDB.status_code,"Lyricsovh_status_code": rLyrics.status_code}

def get_id_artist(artist_name : str):
    """on retourne l'identifiant de l'artiste par son nom 

    Args:
        artist_name (str): le nom de l'artiste

    Returns:
        _type_: id de l'artiste
    """
    try:
           url = Audio_DB+searchArtist
           res = requests.get(url+artist_name)
           id_artist = res.json()['artists'][0]['idArtist']
           return id_artist
    except:
          return("ArtistNotFound")
      
      
      
def get_id_album(artist_name : str): 
    """methode qui génére aléatoirement un album d'un artiste à partir de son nom"""
    id_artist = get_id_artist(artist_name)
    try:
           url = Audio_DB+searchAlbum
           res = requests.get(url+str(id_artist))
           id_album = res.json()['album'][0]['idAlbum']
           return id_album
    except:
        return("AlbumNotFound")

def get_random_track(artist_name : str):
    """methode qui génére les infomations sur la chanson d'un artiste"""
    id_album = get_id_album(artist_name)

    try:
        url=Audio_DB+SearchTrack
        res=requests.get(url+str(id_album))
        tracks=res.json()['track']
        track = random.choice(tracks)
        strArtist = track["strArtist"]
        title = track["strTrack"]
        if track["strMusicVid"]:
            suggested_youtube_url = track["strMusicVid"]
        else:
            suggested_youtube_url="pas de lien"
        songs = {}
        songs ['artist'] = strArtist
        songs['title'] = title
        songs['suggested_youtube_url'] = suggested_youtube_url
        return songs

    except:
        return("TrackNotFound")
  
def get_lyrics(artist_name :str,title : str):
    """methode qui retourne lyrics d'une chanson donnee 

    Args:
        artist_name (str): nom de l'artiste
        title (str): titre du chanson

    Returns:
        _type_: dictionnaire
    """
    try:
        res = requests.get(Lyricsovh + artist_name + "/" + title)
        return res.json()["lyrics"]
    except:
        return("lyricsNotFound")
 
@app.get("/random/{artist_name}")
def get_music_information(artist_name:str):
         id_artist=get_id_artist(artist_name)
         if id_artist!="ArtistNotFound":
            id_album = get_id_album(artist_name)
            if id_album!="AlbumNotFound":
               track = get_random_track(artist_name)
               lyrics = get_lyrics(artist_name , track["title"])
               if lyrics != "lyricsNotFound":
                    track["lyrics"] = lyrics
               else:
                   track["lyrics"]="Pas de lyrics"
               return track
            else:
                return id_album
         else:
             return id_artist

if __name__ == "__main__" :
    uvicorn.run("main:app", host="127.0.0.1", port=8000)
