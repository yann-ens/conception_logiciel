# Serveur

Ce sujet s'appuie sur l'utilisation de 2 API externes :

 : 
- [ ] [AudioDB](https://www.theaudiodb.com/api_guide.php) grand webservice servant de banque de données sur les informations sur des artistes, albums, titres.

- [ ] [Lyricsovh](https://lyricsovh.docs.apiary.io/) permet pour un artiste, et un titre de trouver les paroles associées au titre.

Pour réaliser cela, il était attendu l'implémentation d'un web-service dédié, ce que nous faisons ici :D

Les fonctionnalités développées sont:

- [] Pour un artiste choisi, donne aléatoirement les informations d'une chanson.
- [] Vérifie la configuration et disponibilité des api

Le code implémentant ce service est dans le dossier Serveur. Le fichier .env générique liste également les différents liens d'accès aux différentes API. 

Pour entrainer le webservice, le bout de code suivant fera l'affaire :
```
cd serveur
python main.py
```
